# i3wm config files

<h2> MOVED TO https://gitlab.com/wiokru/dotfiles <h2>


My i3 config version (modified version from ArcoLinux)

Required:

- Font Awesome
- sp (https://gist.github.com/wandernauta/6800547) or playerctl (https://github.com/altdesktop/playerctl) - choose at scripts/spotify_info.sh
- acpi
- rofi
- termite
- kitty
- i3lock (https://github.com/tpb1908/i3lock-color)
- i3blocks
- i3blocks-contrib
